-- EXAMPLE
local on_attach = require("nvchad.configs.lspconfig").on_attach
local on_init = require("nvchad.configs.lspconfig").on_init
local capabilities = require("nvchad.configs.lspconfig").capabilities

local lspconfig = require "lspconfig"
local util = require "lspconfig/util"
local servers = { "html", "cssls", "clangd", "tailwindcss" }

-- lsps with default config
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    on_init = on_init,
    capabilities = capabilities,
  }
end

-- typescript
lspconfig.ts_ls.setup {
  on_attach = on_attach,
  on_init = on_init,
  capabilities = capabilities,
}

--javascript
lspconfig.quick_lint_js.setup {}

-- Golang
-- Gopls
lspconfig.gopls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  cmd = { "gopls" },
  filetypes = { "go", "gomod", "gowork", "gotmpl" },
  root_dir = util.root_pattern("go.work", "go.mod", ".git"),
  settings = {
    gopls = {
      completeUnimported = true,
      usePlaceholders = true,
      analyses = {
        unusedparams = true,
      },
    },
  },
}

--Golangci
local configsGL = require "lspconfig/configs"
if not configsGL.golangcilsp then
  configsGL.golangcilsp = {
    default_config = {
      cmd = { "golangci-lint-langserver" },
      root_dir = lspconfig.util.root_pattern(".git", "go.mod"),
      init_options = {
        command = {
          "golangci-lint",
          "run",
          "--enable-all",
          "--disable",
          "lll",
          "--out-format",
          "json",
          "--issues-exit-code=1",
        },
      },
    },
  }
end

lspconfig.golangci_lint_ls.setup {
  filetypes = { "go", "gomod" },
}
-- Eslint-lsp
lspconfig.eslint.setup {
  filetypes = { "typescriptreact", "typescript", "javascriptreact", "javascript" },
}

-- GraphQl
lspconfig.graphql.setup {
  filetypes = { "graphql", "typescriptreact", "javascriptreact", "javascript" },
  root_dir = util.root_pattern(".graphqlconfig", ".graphqlrc", "package.json"),
}

--Bash
lspconfig.bashls.setup {}
